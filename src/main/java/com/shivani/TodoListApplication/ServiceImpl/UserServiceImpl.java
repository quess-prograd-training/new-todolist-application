package com.shivani.TodoListApplication.ServiceImpl;

import com.shivani.TodoListApplication.Entity.Role;
import com.shivani.TodoListApplication.Entity.Users;
import com.shivani.TodoListApplication.Payload.UserDto;
import com.shivani.TodoListApplication.Repository.RoleRepository;
import com.shivani.TodoListApplication.Repository.UserRepository;
import com.shivani.TodoListApplication.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

   /* @Override
    public UserDto createUser(UserDto userDto) {
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        Users users = userDtoToEntity(userDto); //converted userDto to Users Entity
        Users savedUser= userRepository.save(users);
        return entityToUserDto(savedUser);
    }

    */
   @Override
   public UserDto createUser(UserDto userDto) {
       userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
       Users users = userDtoToEntity(userDto); //converted userDto to Users Entity
       List<Users> user = userRepository.findAll();
       Role roles = roleRepository.findById(2L).orElseThrow(() -> new RuntimeException("Role with ID 2 not found"));
       Set<Role> roleObj = new HashSet<>();
       if (!user.isEmpty()) { //if user available then make role as normal user
           roleObj.add(roles);
       } else {
           Role userRole = roleRepository.findById(1L).orElseThrow(() -> new RuntimeException("Role with ID 1 not found")); // assuming role with id 1 is for "ADMIN"
           roleObj.add(userRole);
       }
       users.setRoles(roleObj);
       Users savedUser = userRepository.save(users);
       return entityToUserDto(savedUser);
   }

    private Users userDtoToEntity(UserDto userDto){
        Users users = new Users();
        users.setName(userDto.getName());
        users.setEmail(userDto.getEmail());
        users.setPassword(userDto.getPassword());
        return users;
    }
    private UserDto entityToUserDto(Users savedUser){
        UserDto userDto = new UserDto();
        userDto.setId(savedUser.getId());
        userDto.setEmail(savedUser.getEmail());
        userDto.setPassword(savedUser.getPassword());
        userDto.setName(savedUser.getName());
        return userDto;
    }

    @Override
    public List<Users> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public String deleteUserById(Long userId) {
        Users existingUser=userRepository.findById(userId).get();
        existingUser.getRoles().clear();//first clear role before delete due to foreign key constraint
        userRepository.delete(userRepository.findById(userId).get());
        return "Deleted user id: "+userId;
    }
}
