package com.shivani.TodoListApplication.ServiceImpl;

import com.shivani.TodoListApplication.Entity.TodoList;
import com.shivani.TodoListApplication.Entity.Users;
import com.shivani.TodoListApplication.Exceptions.APIException;
import com.shivani.TodoListApplication.Exceptions.TodoListNotFound;
import com.shivani.TodoListApplication.Exceptions.UserNotFound;
import com.shivani.TodoListApplication.Payload.TodoListDto;
import com.shivani.TodoListApplication.Repository.TodoListRepository;
import com.shivani.TodoListApplication.Repository.UserRepository;
import com.shivani.TodoListApplication.Service.TodoListService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoListServiceImpl implements TodoListService {

    @Autowired
    TodoListRepository todoListRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;
    @Override
    public TodoListDto saveTodoList(Long userId, TodoListDto todoListDto) {
        Users users=userRepository.findById(userId).orElseThrow(()-> new UserNotFound(String.format("UserId %d not found",userId)));
        TodoList todoList = modelMapper.map(todoListDto,TodoList.class);
        todoList.setFkUser(users);
        //after setting user we are storing data in database
        TodoList savedTodo= todoListRepository.save(todoList);

        return modelMapper.map(savedTodo,TodoListDto.class);
    }

    /*@Override
    public List<TodoListDto> getAllTodoLists(Long userId) {
        userRepository.findById(userId).orElseThrow(
                () -> new UserNotFound(String.format("UserId %d not found",userId))
        );
        List<TodoList> todoLists = todoListRepository.findAllByFkUser(userId);
        return todoLists.stream().map(
                todoList -> modelMapper.map(todoList,TodoListDto.class)
        ).collect(Collectors.toList());
    }

     */
    @Override
    public List<TodoList> getAllTodoLists(Long userId) {
        Users users=userRepository.findById(userId).orElseThrow(
                () -> new UserNotFound(String.format("UserId %d not found",userId))
        );
        return todoListRepository.findAllByFkUser(users);
    }


    @Override
    public TodoListDto getTodo(Long userId, Long todoId) { //this method works fine with postman
        Users users=userRepository.findById(userId).orElseThrow(()-> new UserNotFound(String.format("UserId %d not found",userId)));
        TodoList todoList=todoListRepository.findById(todoId).orElseThrow(
                ()-> new TodoListNotFound(String.format("TodoId %d not found",todoId))
        );
        if(users.getId() != todoList.getFkUser().getId()){
            throw new APIException(String.format("TodoId %d is not belongs to userId %d",todoId,userId));

        }
        return modelMapper.map(todoList, TodoListDto.class);
    }

    @Override
    public void deleteTodo(Long userId, Long todoId) { //this method works fine with postman
        Users users=userRepository.findById(userId).orElseThrow(()-> new UserNotFound(String.format("UserId %d not found",userId)));
        TodoList todoList=todoListRepository.findById(todoId).orElseThrow(
                ()-> new TodoListNotFound(String.format("TodoId %d not found",todoId))
        );
        if(users.getId() != todoList.getFkUser().getId()){
            throw new APIException(String.format("TodoId %d is not belongs to userId %d",todoId,userId));

        }
        todoListRepository.deleteById(todoId); //delete todo
    }

}
