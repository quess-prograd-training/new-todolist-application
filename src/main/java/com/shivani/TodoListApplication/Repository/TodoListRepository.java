package com.shivani.TodoListApplication.Repository;

import com.shivani.TodoListApplication.Entity.TodoList;
import com.shivani.TodoListApplication.Entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoListRepository extends JpaRepository<TodoList,Long> {



    List<TodoList> findAllByFkUser(Users users);
}
