package com.shivani.TodoListApplication.Repository;

import com.shivani.TodoListApplication.Entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Long> {
}
