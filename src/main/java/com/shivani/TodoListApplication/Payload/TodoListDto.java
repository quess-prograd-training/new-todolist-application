package com.shivani.TodoListApplication.Payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TodoListDto {
    private Long id;
    private String todoName;
    private boolean completed;
}
