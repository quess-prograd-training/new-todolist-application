package com.shivani.TodoListApplication.Controller;

import com.shivani.TodoListApplication.Entity.Users;
import com.shivani.TodoListApplication.Payload.LoginDto;
import com.shivani.TodoListApplication.Payload.UserDto;
import com.shivani.TodoListApplication.Repository.UserRepository;
import com.shivani.TodoListApplication.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;


    @PreAuthorize("permitAll()")
    @PostMapping("/register")
    public UserDto createUser(@RequestBody UserDto userDto){
        return userService.createUser(userDto);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    @PostMapping("/login")
    public String loginUser(@RequestBody LoginDto loginDto){
        Authentication authentication =
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(loginDto.getEmail(),loginDto.getPassword())
                );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return ("User logged in successfully!!");
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/displayAll")
    public List<Users> getAllUsers(){
        return userService.getAllUsers();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping("/{userId}")
    public void deleteUser(@Valid @PathVariable Long userId) {
        userService.deleteUserById(userId);
    }


}
