package com.shivani.TodoListApplication.Controller;

import com.shivani.TodoListApplication.Entity.TodoList;
import com.shivani.TodoListApplication.Payload.TodoListDto;
import com.shivani.TodoListApplication.Service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TodoListController {

    @Autowired
    private TodoListService todoListService;

    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    @PostMapping("/{userId}/todos")
    public TodoListDto saveTodoList(
            @PathVariable(name ="userId") Long userId,
            @RequestBody TodoListDto todoListDto
          ){
        return todoListService.saveTodoList(userId,todoListDto);
    }

    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/{userId}/todos")
    public List<TodoList> getAllTTodoLists(
            @PathVariable(name = "userId") Long userId){
        return todoListService.getAllTodoLists(userId);
    }


    @PreAuthorize("hasAnyAuthority('ADMIN','USER')")
    @GetMapping("/{userId}/todos/{todoId}")
    public TodoListDto getTodo(
            @PathVariable(name = "userId") Long userId,
            @PathVariable(name = "todoId") Long todoId
             ){
        return todoListService.getTodo(userId,todoId);
    }

    //delete individual todo
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping("/{userId}/todos/{todoId}")
    public String deleteTodo(
            @PathVariable(name = "userId") Long userId,
            @PathVariable(name = "todoId") Long todoId
         ){
        todoListService.deleteTodo(userId,todoId);
        return "Todo deleted successfully!!!";
    }
}
