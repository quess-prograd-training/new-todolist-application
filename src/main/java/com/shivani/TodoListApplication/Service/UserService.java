package com.shivani.TodoListApplication.Service;

import com.shivani.TodoListApplication.Entity.Users;
import com.shivani.TodoListApplication.Payload.UserDto;

import java.util.List;

public interface UserService {
    UserDto createUser(UserDto userDto);

    List<Users> getAllUsers();

    String deleteUserById(Long userId);
}
