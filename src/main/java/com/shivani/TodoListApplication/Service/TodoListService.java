package com.shivani.TodoListApplication.Service;

import com.shivani.TodoListApplication.Entity.TodoList;
import com.shivani.TodoListApplication.Payload.TodoListDto;

import java.util.List;

public interface TodoListService {
    TodoListDto saveTodoList(Long userId, TodoListDto todoListDto);

    List<TodoList> getAllTodoLists(Long userId);

    TodoListDto getTodo(Long userId, Long todoId);

    void deleteTodo(Long userId, Long todoId);
}
