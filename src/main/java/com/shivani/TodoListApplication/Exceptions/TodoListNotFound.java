package com.shivani.TodoListApplication.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TodoListNotFound extends RuntimeException{
    private String message;

    public TodoListNotFound(String message){
        super(message);
        this.message=message;
    }
}
